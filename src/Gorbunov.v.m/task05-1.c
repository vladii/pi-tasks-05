#include <stdio.h>
#include <string.h>
#define M 256

FILE *fp;
struct BOOK {
	char title[M];
	char author[M];
	int year;
};


int main() {
	fp = fopen("file.txt", "r");
	if (fp == NULL){
		perror("file.txt:");
		return 1;
	}
	
	struct BOOK* book = (struct BOOK*)malloc(sizeof(struct BOOK));
	char s[M];
	int n = 0;

	while (fgets(s, M, fp)){
		++n;
		book = (struct BOOK*)realloc(book, n * sizeof(struct BOOK));
		book[n - 1].year = 0;
		int i = 0;
		int j = 0;
		while (s[i] != '\t') book[n - 1].title[++j] = s[++i];
		book[n - 1].title[j] = '\0';
		++i;
		j = 0;
		while (s[i]!='\t') book[n - 1].author[++j] = s[++i];
		book[n - 1].author[j] = '\0';
		++i;
		book[n - 1].year = 0;
		while (s[i] != '\n')
			book[n - 1].year = book[n - 1].year * 10 + s[++i] - '0';

	}
	for (int i = 0; i < n; ++i) printf("%s\t%s\t%i\n", book[i].title, book[i].author, book[i].year);
	int newid = 0, oldid = 0;
	for (int i = 0; i < n; ++i) {
		if (book[i].year > book[newid].year)
			newid = i;
		if (book[i].year < book[oldid].year)
			oldid = i;
	}
	printf("The oldest book: %s\t%s\t%i\n", book[oldid].title, book[oldid].author, book[oldid].year);
	printf("The newest book: %s\t%s\t%i\n", book[newid].title, book[newid].author, book[newid].year);
	for (int i = 0; i < n; ++i) {
		int imin = i;
		for (int j = i + 1; j < n; ++j) {
			if (strcmp(book[j].author, book[imin].author) < 0) imin = j;
		}
		struct BOOK k = book[i];
		book[i] = book[imin];
		book[imin] = k;
	}
	for (int i = 0; i < n; ++i) printf("%s\t%s\t%i\n", book[i].title, book[i].author, book[i].year);
	return 0;
	free(book);
}