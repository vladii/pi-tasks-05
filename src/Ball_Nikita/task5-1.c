#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
FILE *file;
#define U 30
#define P 30


int main() 
{
    char arr[256];
    int top, bot, avt2, avt = 0;
    int j = 0;
    int max = 0;
    int min = 0;
    struct BOOK 
    {
        char work[U];
        char author[P];
        int year;
    };
    struct BOOK* array = (struct BOOK*)malloc(sizeof(struct BOOK)*U);
    file = fopen("file.txt", "r");
    
    if (file == NULL) 
    {
        perror("file.txt:");
        return 1;
    }
    
    while (fgets(arr, 256, file)) 
    {
        printf("%s", arr);
        avt2 = 0;
        
        while (arr[avt] != ' ') 
        {
            array[j].author[avt2] = arr[avt];
            avt++;
            avt2++;
        }
        array[j].author[avt2] = '\0';
        avt++;
        avt2 = 0;
        
        while (arr[avt] != ' ') 
        {
            array[j].work[avt2] = arr[avt];
            avt++;
            avt2++;
        }
        array[j].work[avt2] = '\0';
        avt++;
        array[j].year = 0;
        while (arr[avt] != '\n') 
        {
            array[j].year = array[j].year * 10 + arr[avt] - '0';
            avt++;
        }
        if (max == 0)  
            max = array[j].year; top = j; 
        
        if (array[j].year >= max) 
        {
            max = array[j].year;
            top = j;
        }
        
        if (min == 0)  
            min = array[j].year; bot = j; 
     
        if (array[j].year <= min) 
        {
            min = array[j].year;
            bot = j;
        }
        j++;
        avt = 0;
    }
    puts("");
    printf("new: %s, %s, %d\n", array[top].author, array[top].work, array[top].year);
    printf("old: %s, %s, %d\n", array[bot].author, array[bot].work, array[bot].year);
    puts("");
    for (avt = 0; avt<26; avt++)
        for (avt2 = 0; avt2<j; avt2++)
            if (array[avt2].author[0] == 'A' + avt)
                printf("%s, %s, %d\n ", array[avt2].author, array[avt2].work, array[avt2].year);
    fclose(file);
    return 0;
}