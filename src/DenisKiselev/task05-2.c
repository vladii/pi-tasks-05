#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 128

struct BOOK
{
	char name[N];
	char author[N];
	int date;
};

int qtStrings(FILE* fp)	//�-�� �������� ���-�� ����� � txt
{
	int qt=0;
	char buf[N];
	while (fgets(buf, N, fp))
		qt++;
	rewind(fp);
	if ((qt%3!=0) || (qt==0)) return NULL;
	return qt;
}

void getBooks(FILE *fp, struct BOOK *books, int qt)
{
	int i, j=0;
	char buf[N];
	for (i=0; i<qt/3; i++)
	{
		fgets(books[i].name, N, fp);
		fgets(books[i].author, N, fp);
		fgets(buf, N, fp);
		books[i].date=atoi(buf);
	}
}

void outputInf(struct BOOK *books, int qt)
{
	int i;
	int maxInd=0, minInd=0;;
	for (i=0; i<qt/3; i++)
	{
		if (books[i].date > books[maxInd].date)
			maxInd=i;
		if (books[i].date < books[minInd].date)
			minInd=i;
	}
	printf("The newest book from the list:\n%s%s%d\n\n", books[maxInd].name, books[maxInd].author, books[maxInd].date);
	printf("The oldest book from the list:\n%s%s%d\n\n", books[minInd].name, books[minInd].author, books[minInd].date);
}

int main()
{
	FILE* fp;
	int qtStr;
	struct BOOK *books;
	if ((fp=fopen("C:\\books.txt", "r"))==NULL)	
	{
		perror("Error: ");
		return 1;
	}
	qtStr=qtStrings(fp);
	if (qtStr==NULL)
	{
		printf("Error: check the correction of information in your text file.\n");
		return 1;
	}
	books=(struct BOOK*)malloc((qtStr/3)*sizeof(struct BOOK));	//��������� ������ � ������ ���-�� ����
	getBooks(fp, books, qtStr);
	outputInf(books, qtStr);
	fclose(fp);
	free(books);
	return 0;
}
